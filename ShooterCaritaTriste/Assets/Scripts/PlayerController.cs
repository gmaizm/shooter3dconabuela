using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 3f;

    [SerializeField]
    private float m_RotationSpeed = 180f;
    private float m_MouseSensitivity = 1f;

    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    //Camera
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private InputActionAsset InputAsset;
    private InputActionAsset Input;
    private InputAction MovementAction;
    private InputAction CameraMovementAction;


    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private GameObject SecCamera;
    [SerializeField]
    private GameObject CamBox;
    [SerializeField]
    public float LimitAngleX = 10f;
    private float AngleX;

    public int HealthPoints;

    [SerializeField]
    private GameEvent PlayerHit;


    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;

        m_Rigidbody = GetComponent<Rigidbody>();
        Input = Instantiate(InputAsset);
        MovementAction = Input.FindActionMap("Standard").FindAction("Movement");
        CameraMovementAction = Input.FindActionMap("Standard").FindAction("Camera");
        Input.FindActionMap("Standard").FindAction("Shoot1").performed += Shoot1;
        Input.FindActionMap("Standard").FindAction("Shoot2").performed += Shoot2;
        Input.FindActionMap("Standard").FindAction("SecCamera").performed += ChangeToSecCamera;
        Input.FindActionMap("Standard").Enable();

        if (TryGetComponent<IDamageable>(out IDamageable damageable))
            damageable.OnDamage += ReceiveDamage;

    }

    void Update()
    {
        //transform.Rotate(Vector3.up * CameraMovementAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime);

        m_Movement = Vector3.zero;

        m_Movement = MovementAction.ReadValue<Vector2>();
        /*
           if (m_Camera.transform.eulerAngles.x > -60 && m_Camera.transform.eulerAngles.x < 60)
           {
               m_Camera.transform.Rotate((m_InvertY ? 1 : -1) * Vector3.right * CameraMovementAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime);
           } 
        */
        m_Camera.transform.Rotate((m_InvertY ? 1 : -1) * Vector3.right * CameraMovementAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime);

        Vector3 mouseX = Vector3.up * CameraMovementAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime;
        float mouseY = -CameraMovementAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime;
        float angles = m_Camera.transform.localEulerAngles.x + mouseY;
        AngleX = Mathf.Clamp(angles, -LimitAngleX, LimitAngleX);
        transform.Rotate(mouseX);
        m_Camera.transform.localEulerAngles = Vector3.right * angles;




    }

    private void FixedUpdate()
    {
        //m_Rigidbody.MovePosition(transform.position + m_Movement.normalized * m_Speed * Time.fixedDeltaTime);
        m_Rigidbody.velocity = (transform.forward * m_Movement.y + transform.right * m_Movement.x) * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
    }

    private void Shoot(Vector3 pos)
    {
        
        RaycastHit hit;
        if (Physics.Raycast(pos, m_Camera.transform.forward, out hit, 20f, m_ShootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(10);

            if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                pushable.Push(m_Camera.transform.forward, 10);
        }
    }

    private void Shoot1(InputAction.CallbackContext actionContext)
    {

        Shoot(m_Camera.transform.position);
    }

    private void Shoot2(InputAction.CallbackContext actionContext)
    {

        for (int i = 0; i < 5;  i++)
        {
            float rx = Random.Range(-0.5f, 0.5f);
            float ry = Random.Range(-0.5f, 0.5f);
            Vector3 pos = m_Camera.transform.position + m_Camera.transform.up*ry + m_Camera.transform.right * rx;
            Shoot(pos);
        }
    }

    private void ChangeToSecCamera(InputAction.CallbackContext actionContext)
    {
        Input.FindActionMap("Standard").Disable();
        SecCamera.GetComponent<Camera>().enabled = true;
        m_Camera.GetComponent<Camera>().enabled = false;
        CamBox.GetComponent<CamController>().EnableInputs();
    }

    public void EnableInputs()
    {
        Input.FindActionMap("Standard").Enable();
    }

    private void ReceiveDamage(int damage)
    {
        HealthPoints -= damage;
        if (HealthPoints <= 0)
            Die();

        PlayerHit.Raise();
    }

    private void Die()
    {
        if (gameObject.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;

        Activate(true);
    }

    public void Activate(bool state)
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = !state;
        gameObject.GetComponent<Animator>().enabled = !state;
    }

}
