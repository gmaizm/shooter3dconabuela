using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEditor.PlayerSettings;

public class EnemyController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, CHASE, ATTACK, PATROLL, DAMAGED };
    private SwitchMachineStates CurrentState;
    [SerializeField]
    public Vector3[] waypoints = new Vector3[2];
    [SerializeField]
    private int target = 0;
    private bool returnpatroll = false;
    [SerializeField]
    private LayerMask m_ShootMask;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == CurrentState)
            return;

        ExitState();
        InitState(newState);
    }
    private void InitState(SwitchMachineStates currentState)
    {
        CurrentState = currentState;
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                Debug.Log("Me paro");
                Animator.Play("idle_selfcheck_1_300f");

                break;

            case SwitchMachineStates.CHASE:

                Debug.Log("Chase es el mejor opening de jojos");
                Animator.Play("locom_f_jogging_30f");

                break;
            case SwitchMachineStates.ATTACK:
                gameObject.GetComponent<NavMeshAgent>().ResetPath();

                StartCoroutine(shoot());
            
                Animator.Play("Exercise_warmingUp_170f");

                break;

            case SwitchMachineStates.PATROLL:

                Debug.Log("Patrullando mi pana");
                Animator.Play("locom_f_basicWalk_30f");
                gameObject.GetComponent<NavMeshAgent>().SetDestination(waypoints[target]);

                break;
            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }
    private void ExitState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.CHASE:
                break;
            case SwitchMachineStates.ATTACK:
                StopAllCoroutines();
                break;
            case SwitchMachineStates.PATROLL:
                break;
            case SwitchMachineStates.DAMAGED:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.CHASE);
                if (returnpatroll)
                {
                    returnpatroll = false;
                    StartCoroutine(confuse());
                }

                break;

            case SwitchMachineStates.CHASE:
                gameObject.GetComponent<NavMeshAgent>().SetDestination(tjugador);

                if (!this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.IDLE);
                returnpatroll = true;

                if (this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                    ChangeState(SwitchMachineStates.ATTACK);

                break;

        case SwitchMachineStates.ATTACK:
            if (!this.gameObject.GetComponentInChildren<AttackDetection>().detectaattack)
                ChangeState(SwitchMachineStates.CHASE);

            break;

            case SwitchMachineStates.PATROLL:
                if (gameObject.GetComponent<NavMeshAgent>().remainingDistance < 0.5f)
                {
                    if (target == waypoints.Length - 1) target = 0;
                    else ++target;
                    gameObject.GetComponent<NavMeshAgent>().SetDestination(waypoints[target]);
                }
                    


                if (this.gameObject.GetComponentInChildren<Detection>().detecta)
                    ChangeState(SwitchMachineStates.CHASE);
                break;
            case SwitchMachineStates.DAMAGED:
                /*
                if (this.HealthPoints <= 0)
                {
                    this.gameObject.SetActive(false);
                }
                else
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }*/


                break;

            default:
                break;
        }
    }
    IEnumerator confuse()
    {
        yield return new WaitForSeconds(1);
        ChangeState(SwitchMachineStates.PATROLL);

    }
    IEnumerator shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            Debug.Log("pium");
            float rx = Random.Range(-0.5f, 0.5f);
            float ry = Random.Range(-0.5f, 0.5f);
            Vector3 pos = transform.position + new Vector3(rx, ry, 0);
            RaycastHit hit;
            if (Physics.Raycast(pos, this.gameObject.transform.forward, out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(gameObject.transform.position, hit.point, Color.green, 2f);

                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(5);

                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(gameObject.transform.forward, 10);
            }

        }

    }
    private Animator Animator;
    private Rigidbody Rigidbody;

    private float m_SQDistancePerFrame;
    private Vector3 tjugador;
    public GameObject player;

    [Header("Enemy Values")]
    [SerializeField]
    private float Speed = 2;
    [SerializeField]
    private int HealthPoints = 10;
    [SerializeField]
    private int DamageBala;
    [SerializeField]
    private float SpeedBala;
    private Rigidbody[] m_Bones;


    void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        tjugador = player.transform.position;
        Animator = GetComponent<Animator>();
        m_SQDistancePerFrame = Speed * Time.fixedDeltaTime;
        m_SQDistancePerFrame *= m_SQDistancePerFrame;

        m_Bones = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage += ReceiveDamage;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.PATROLL);
    }

    void Update()
    {
        tjugador = player.transform.position;
        UpdateState();
    }

    private void ReceiveDamage(int damage)
    {
        HealthPoints -= damage;
        if (HealthPoints <= 0)
            Die();
    }

    private void Die()
    {
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;

        Activate(true);
        ChangeState(SwitchMachineStates.DAMAGED);
        gameObject.GetComponent<NavMeshAgent>().ResetPath();
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        gameObject.GetComponent<Animator>().enabled = !state;
    }


}
