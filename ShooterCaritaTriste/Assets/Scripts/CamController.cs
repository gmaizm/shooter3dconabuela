using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CamController : MonoBehaviour
{

    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private GameObject SecCamera;
    [SerializeField]
    private GameObject Player;

    [SerializeField]
    private InputActionAsset InputAsset;
    private InputActionAsset Input;
    private InputAction CameraMovementAction;

    [SerializeField]
    private float m_RotationSpeed = 180f;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;

        Input = Instantiate(InputAsset);
        CameraMovementAction = Input.FindActionMap("CameraMap").FindAction("CameraMovement");
        Input.FindActionMap("CameraMap").FindAction("ReturnPlayer").performed += ReturnPlayer;


    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * CameraMovementAction.ReadValue<Vector2>().x * m_RotationSpeed * Time.deltaTime);
        transform.Rotate(-Vector3.right * CameraMovementAction.ReadValue<Vector2>().y * m_RotationSpeed * Time.deltaTime);
    }
    private void ReturnPlayer(InputAction.CallbackContext actionContext)
    {
        Input.FindActionMap("CameraMap").Disable();
        SecCamera.GetComponent<Camera>().enabled = false;
        m_Camera.GetComponent<Camera>().enabled = true;
        Player.GetComponent<PlayerController>().EnableInputs();
    }

    public void EnableInputs()
    {
        
        Input.FindActionMap("CameraMap").Enable();
    }

}
